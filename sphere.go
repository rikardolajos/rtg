package main

import (
	"math"
)

// Sphere is an object for the scene.
type Sphere struct {
	center   Vec3
	radius   float64
	material Material
}

// Hit calculates if the ray hits the sphere.
func (s Sphere) Hit(r Ray, tMin, tMax float64, rec *HitRecord) bool {
	oc := r.origin.Sub(s.center)

	a := r.direction.Norm2()
	b := oc.Dot(r.direction)
	c := oc.Norm2() - s.radius*s.radius

	discriminant := b*b - a*c
	if discriminant > 0 {

		temp := (-b - math.Sqrt(discriminant)) / a
		if tMin < temp && temp < tMax {
			rec.t = temp
			rec.p = r.PointAtT(temp)
			rec.normal = rec.p.Sub(s.center).Mul(1 / s.radius)
			rec.material = s.material
			return true
		}

		temp = (-b + math.Sqrt(discriminant)) / a
		if tMin < temp && temp < tMax {
			rec.t = temp
			rec.p = r.PointAtT(temp)
			rec.normal = rec.p.Sub(s.center).Mul(1 / s.radius)
			rec.material = s.material
			return true
		}
	}
	return false
}

// BoundingBox for the sphere geometry.
func (s Sphere) BoundingBox(t0, t1 float64, box *AABB) bool {
	*box = AABB{s.center.Sub(Vec3{s.radius, s.radius, s.radius}), s.center.Add(Vec3{s.radius, s.radius, s.radius})}
	return true
}
