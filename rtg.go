package main

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math"
	"math/rand"
	"os"
	"runtime"
	"sync"
	"time"
)

const width = 200
const height = 100
const superSampling = 10
const rayDepth = 5

var sky = Vec3{0.5, 0.7, 1.0}
var fog = Vec3{1.0, 1.0, 1.0}

func geometryHit(world []Geometry, r Ray, tMin, tMax float64, rec *HitRecord) bool {
	var tempRec HitRecord
	var hit bool
	closest := tMax

	for _, geom := range world {
		if geom.Hit(r, tMin, closest, &tempRec) {
			hit = true
			closest = tempRec.t
			*rec = tempRec
		}
	}

	return hit
}

func rayColor(r Ray, world BVHNode, depth int) Vec3 {
	// Look for geometry.
	var rec HitRecord
	//if geometryHit(world, r, 0.001, math.MaxFloat64, &rec) {
	if world.Hit(r, 0.001, math.MaxFloat64, &rec) {
		valid, attenuation, scattered := rec.material.Scatter(r, rec)
		if depth < rayDepth && valid {
			return attenuation.MulElement(rayColor(scattered, world, depth+1))
		}
		return Vec3{0.0, 0.0, 0.0}
	}

	// No geometry hit, render sky.
	unitDirection := r.direction.Unit()
	t := 0.5*unitDirection.Y + 0.5
	return Lerp(fog, sky, t)
}

func renderRow(wg *sync.WaitGroup, j, width int, cam Camera, world BVHNode, renderImage *image.NRGBA) {
	defer wg.Done()
	for i := 0; i < width; i++ {
		// Antialias by random super sampling.
		col := Vec3{0.0, 0.0, 0.0}
		for s := 0; s < superSampling; s++ {
			u := (float64(i) + rand.Float64()) / float64(width)
			v := (float64(j) + rand.Float64()) / float64(height)
			r := cam.GetRay(u, v)
			col = col.Add(rayColor(r, world, 0))
		}
		col = col.Mul(1.0 / float64(superSampling))

		// Simple gamma 2 correction.
		col = Vec3{math.Sqrt(col.X), math.Sqrt(col.Y), math.Sqrt(col.Z)}

		// Convert colors to bytes and write to image.
		var ir = byte(255.99 * col.X)
		var ig = byte(255.99 * col.Y)
		var ib = byte(255.99 * col.Z)
		renderImage.Set(i, height-j, color.NRGBA{ir, ig, ib, 255})
	}

}

func scene() []Geometry {
	n := 500
	world := make([]Geometry, 0, n)
	world = append(world, Sphere{Vec3{0.0, -1000.0, -1.0}, 1000.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})

	for x := -11; x < 11; x++ {
		for z := -11; z < 11; z++ {
			center := Vec3{float64(x) + 0.9*rand.Float64(), 0.205, float64(z) + 0.9*rand.Float64()}
			if center.Norm() < 1.5 {
				continue
			}
			if center.Sub(Vec3{4.0, 0.2, 0.0}).Norm() > 0.9 {
				world = append(world, Sphere{center, 0.2, Dielectric{Vec3{(20.0 + 8*rand.Float64()) / 255.0, (111.0 + 30*rand.Float64()) / 255.0, (28.0 + 9*rand.Float64()) / 255.0},
					1.5}})
			}
		}
	}

	world = append(world, Sphere{Vec3{0.0, 1.0, 0.0}, 1.0, Metal{Vec3{0.7, 0.6, 0.5}, 0.0}})

	// n := 500
	// world := make([]Geometry, 0, n)
	// world = append(world, Sphere{Vec3{0.0, 0.0, 0.0}, 1.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})
	// world = append(world, Sphere{Vec3{2.0, 0.0, 0.0}, 1.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})
	// world = append(world, Sphere{Vec3{0.0, 0.0, 2.0}, 1.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})
	// world = append(world, Sphere{Vec3{3.0, 0.0, 3.0}, 1.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})
	// world = append(world, Sphere{Vec3{-4.0, 0.0, 0.0}, 1.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})
	// world = append(world, Sphere{Vec3{0.0, 0.0, -4.0}, 1.0, Lambertian{Vec3{0.5, 0.5, 0.5}}})

	return world
}

func main() {
	runtime.GOMAXPROCS(6)

	renderImage := image.NewNRGBA(image.Rect(0, 0, width, height))

	world := InitBVH(scene())
	//world := scene()

	position := Vec3{15.0, 2.0, 4.0}
	lookAt := Vec3{0.0, 0.0, 0.0}
	aspect := float64(width) / float64(height)
	focusDistance := position.Sub(Vec3{0.0, 0.0, 0.0}).Norm()
	aperature := 0.05
	cam := NewCamera(position, lookAt, Vec3{0.0, 1.0, 0.0}, 15, aspect, aperature, focusDistance)

	start := time.Now()

	var wg sync.WaitGroup
	for j := height - 1; j >= 0; j-- {
		wg.Add(1)
		go renderRow(&wg, j, width, cam, world, renderImage)
	}
	wg.Wait()

	fmt.Printf("Rendering took: %v", time.Since(start))

	file, err := os.Create("render.png")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	png.Encode(file, renderImage)
	writer.Flush()
}
