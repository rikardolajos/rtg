package main

import (
	"math"
)

// Vec3 is suitable for storing points, vectors or colors.
type Vec3 struct {
	X, Y, Z float64
}

// Elements returns an array with the elements.
func (v0 Vec3) Elements() [3]float64 {
	return [3]float64{v0.X, v0.Y, v0.Z}
}

// Add adds v0 and v1.
func (v0 Vec3) Add(v1 Vec3) Vec3 {
	return Vec3{v0.X + v1.X, v0.Y + v1.Y, v0.Z + v1.Z}
}

// Sub subtracts v1 from v0.
func (v0 Vec3) Sub(v1 Vec3) Vec3 {
	return Vec3{v0.X - v1.X, v0.Y - v1.Y, v0.Z - v1.Z}
}

// Mul multiplies m to each element of v0.
func (v0 Vec3) Mul(m float64) Vec3 {
	return Vec3{m * v0.X, m * v0.Y, m * v0.Z}
}

// MulElement makes a elementwise multiplication between two vectors.
func (v0 Vec3) MulElement(v1 Vec3) Vec3 {
	return Vec3{v0.X * v1.X, v0.Y * v1.Y, v0.Z * v1.Z}
}

// Dot calculates to dot product of v0 and v1.
func (v0 Vec3) Dot(v1 Vec3) float64 {
	return v0.X*v1.X + v0.Y*v1.Y + v0.Z*v1.Z
}

// Cross calculates to cross product of v0 and v1.
func (v0 Vec3) Cross(v1 Vec3) Vec3 {
	return Vec3{v0.Y*v1.Z - v0.Z*v1.Y,
		v0.Z*v1.X - v0.X*v1.Z,
		v0.X*v1.Y - v0.Y*v1.X}
}

// Norm calculates the norm of v0.
func (v0 Vec3) Norm() float64 {
	return math.Sqrt(v0.Dot(v0))
}

// Norm2 calculates the squared norm of v0.
func (v0 Vec3) Norm2() float64 {
	return v0.Dot(v0)
}

// Unit returns v0 but with unit length.
func (v0 Vec3) Unit() Vec3 {
	n := v0.Norm()
	if n == 0 {
		return Vec3{0, 0, 0}
	}
	return v0.Mul(1 / n)
}

// Lerp performs linear interpolation between v0 and v1.
func Lerp(v0, v1 Vec3, t float64) Vec3 {
	return v0.Mul(1 - t).Add(v1.Mul(t))
}
