package main

// Geometry is an interface for all hittable geometries in the scene.
type Geometry interface {
	Hit(r Ray, tMin, tMax float64, rec *HitRecord) bool
	BoundingBox(t0, t1 float64, aabb *AABB) bool
}

// HitRecord is a struct for storing previous ray hit.
type HitRecord struct {
	t         float64
	p, normal Vec3
	material  Material
}
