package main

import (
	"math"
	"math/rand"
)

// Camera defines to view position and screen size.
type Camera struct {
	u, v, w                                                Vec3
	origin, screenOrigin, screenHorizontal, screenVertical Vec3
	aperature                                              float64
}

func randomInUnitDisk() Vec3 {
	p := Vec3{rand.Float64(), rand.Float64(), rand.Float64()}.Mul(2.0).Sub(
		Vec3{1.0, 1.0, 0.0})
	for p.Norm2() >= 1.0 {
		p = Vec3{rand.Float64(), rand.Float64(), rand.Float64()}.Mul(2.0).Sub(
			Vec3{1.0, 1.0, 0.0})
	}
	return p
}

// NewCamera returns a Camera struct with default values.
func NewCamera(position, lookAt, up Vec3, vFOV, aspect, aperature, focusDistance float64) Camera {
	theta := vFOV * math.Pi / 180.0
	halfHeight := math.Tan(theta / 2.0)
	halfWidth := aspect * halfHeight

	c := Camera{}

	c.w = position.Sub(lookAt).Unit()
	c.u = up.Cross(c.w).Unit()
	c.v = c.w.Cross(c.u)

	c.origin = position
	c.screenOrigin = c.origin.Sub(
		c.u.Mul(halfWidth * focusDistance)).Sub(
		c.v.Mul(halfHeight * focusDistance)).Sub(
		c.w.Mul(focusDistance))
	c.screenHorizontal = c.u.Mul(2 * halfWidth * focusDistance)
	c.screenVertical = c.v.Mul(2 * halfHeight * focusDistance)

	c.aperature = aperature

	return c
}

// GetRay returns a camera ray for the given uv screen coordinate.
func (c Camera) GetRay(s, t float64) Ray {
	rd := randomInUnitDisk().Mul(c.aperature / 2.0)
	offset := c.u.Mul(rd.X).Add(c.v.Mul(rd.Y))
	return Ray{c.origin.Add(offset), c.screenOrigin.Add(
		c.screenHorizontal.Mul(s)).Add(
		c.screenVertical.Mul(t)).Sub(
		c.origin).Sub(
		offset)}
}
