package main

import "math"

// AABB is an Axis Aligned Bounding Box.
type AABB struct {
	min, max Vec3
}

func overlap(tMin, tMax, minE, maxE, rOriginE, rDirectionE float64) bool {
	invD := 1.0 / rDirectionE
	t0 := (minE - rOriginE) * invD
	t1 := (maxE - rOriginE) * invD
	if invD < 0.0 {
		t0, t1 = t1, t0
	}
	tMin = math.Max(t0, tMin)
	tMax = math.Min(t1, tMax)
	return tMax > tMin
}

// Hit checks if the ray hits the AABB.
func (box AABB) Hit(r Ray, tMin, tMax float64) bool {
	return overlap(tMin, tMax, box.min.X, box.max.X, r.origin.X, r.direction.X) ||
		overlap(tMin, tMax, box.min.Y, box.max.Y, r.origin.Y, r.direction.Y) ||
		overlap(tMin, tMax, box.min.Z, box.max.Z, r.origin.Z, r.direction.Z)
}

// SurroundingBox returns the AABB surrounding both box0 and box1.
func SurroundingBox(box0, box1 AABB) AABB {
	min := Vec3{math.Min(box0.min.X, box1.min.X), math.Min(box0.min.Y, box1.min.Y), math.Min(box0.min.Z, box1.min.Z)}
	max := Vec3{math.Max(box0.max.X, box1.max.X), math.Max(box0.max.Y, box1.max.Y), math.Max(box0.max.Z, box1.max.Z)}
	return AABB{min, max}
}
