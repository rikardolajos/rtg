package main

import (
	"fmt"
	"math/rand"
	"sort"
)

// BVHNode is a node in the bounding volume hierarchy.
type BVHNode struct {
	left, right Geometry
	box         AABB
}

// Sorting functions
type byX []Geometry
type byY []Geometry
type byZ []Geometry

func (a byX) Len() int      { return len(a) }
func (a byY) Len() int      { return len(a) }
func (a byZ) Len() int      { return len(a) }
func (a byX) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a byY) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a byZ) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

func (a byX) Less(i, j int) bool {
	var boxLeft, boxRight AABB
	if !a[i].BoundingBox(0, 0, &boxLeft) || !a[j].BoundingBox(0, 0, &boxRight) {
		fmt.Println("No bounding box in BVHNode")
	}
	return boxLeft.min.X-boxRight.min.X < 0.0
}

func (a byY) Less(i, j int) bool {
	var boxLeft, boxRight AABB
	if !a[i].BoundingBox(0, 0, &boxLeft) || !a[j].BoundingBox(0, 0, &boxRight) {
		fmt.Println("No bounding box in BVHNode")
	}
	return boxLeft.min.Y-boxRight.min.Y < 0.0
}

func (a byZ) Less(i, j int) bool {
	var boxLeft, boxRight AABB
	if !a[i].BoundingBox(0, 0, &boxLeft) || !a[j].BoundingBox(0, 0, &boxRight) {
		fmt.Println("No bounding box in BVHNode")
	}
	return boxLeft.min.Z-boxRight.min.Z < 0.0
}

// InitBVH initializes the bounding volume hierarchy
func InitBVH(list []Geometry) (node BVHNode) {

	// Randomly divide the list along an axis.
	switch axis := rand.Intn(3); axis {
	case 0:
		sort.Sort(byX(list))
	case 1:
		sort.Sort(byY(list))
	case 2:
		sort.Sort(byZ(list))
	}

	// Put each half of the list in the node's children.
	switch len(list) {
	case 1:
		node.left, node.right = list[0], list[0]
	case 2:
		node.left, node.right = list[0], list[1]
	default:
		half := int(len(list) / 2)
		node.left = InitBVH(list[:half])
		node.right = InitBVH(list[half:])
	}

	// Create the box structure.
	var boxLeft, boxRight AABB
	if !node.left.BoundingBox(0, 0, &boxLeft) || !node.right.BoundingBox(0, 0, &boxRight) {
		fmt.Println("No bounding box in BVHNode")
	}
	node.box = SurroundingBox(boxLeft, boxRight)

	return
}

// Hit calculates if the ray hits the bounding box of the BVH node.
func (node BVHNode) Hit(r Ray, tMin, tMax float64, rec *HitRecord) bool {
	if node.box.Hit(r, tMin, tMax) {
		var leftRec, rightRec HitRecord
		hitLeft := node.left.Hit(r, tMin, tMax, &leftRec)
		hitRight := node.right.Hit(r, tMin, tMax, &rightRec)

		if hitLeft && hitRight {
			if leftRec.t < rightRec.t {
				*rec = leftRec
			} else {
				*rec = rightRec
			}
			return true
		} else if hitLeft {
			*rec = leftRec
			return true
		} else if hitRight {
			*rec = rightRec
			return true
		}
	}
	return false
}

// BoundingBox sets the bounding box of the BVH node.
func (node BVHNode) BoundingBox(t0, t1 float64, box *AABB) bool {
	*box = node.box
	// fmt.Println(*box)
	return true
}
