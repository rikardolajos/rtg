package main

// Ray is a ray for ray tracing.
type Ray struct {
	origin, direction Vec3
}

// PointAtT returns the point t along the ray.
func (r Ray) PointAtT(t float64) Vec3 {
	return r.origin.Add(r.direction.Mul(t))
}
