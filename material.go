package main

import (
	"math"
	"math/rand"
)

// Material handles different types for light scattering.
type Material interface {
	Scatter(r Ray, rec HitRecord) (bool, Vec3, Ray)
}

// Lambertian material.
type Lambertian struct {
	albedo Vec3
}

// Metal material.
type Metal struct {
	albedo Vec3
	fuzz   float64
}

// Dielectric material.
type Dielectric struct {
	albedo          Vec3
	refractiveIndex float64
}

func reflect(v, n Vec3) Vec3 {
	return v.Sub(n.Mul(2 * v.Dot(n)))
}

func refract(v, n Vec3, refractiveFactor float64) (bool, Vec3) {
	uv := v.Unit()
	dt := uv.Dot(n)
	discriminant := 1.0 - refractiveFactor*refractiveFactor*(1-dt*dt)
	if discriminant > 0 {
		refracted := uv.Sub(n.Mul(dt)).Mul(refractiveFactor).Sub(n.Mul(math.Sqrt(discriminant)))
		return true, refracted
	}
	return false, Vec3{0.0, 0.0, 0.0}
}

// Schlick's polynomial approximation of specular reflection with Fresnel factor.
func schlick(cosine, refractiveIndex float64) float64 {
	r0 := (1.0 - refractiveIndex) / (1.0 + refractiveIndex)
	r0 = r0 * r0
	return r0 + (1-r0)*math.Pow(1.0-cosine, 5)
}

func randomInUnitSphere() Vec3 {
	p := Vec3{rand.Float64(), rand.Float64(), rand.Float64()}.Mul(2.0).Sub(
		Vec3{1.0, 1.0, 1.0})
	for p.Norm2() >= 1.0 {
		p = Vec3{rand.Float64(), rand.Float64(), rand.Float64()}.Mul(2.0).Sub(
			Vec3{1.0, 1.0, 1.0})
	}
	return p
}

// Scatter funciton for the lambertion material.
func (m Lambertian) Scatter(r Ray, rec HitRecord) (valid bool, attenuation Vec3, scattered Ray) {
	target := rec.p.Add(rec.normal.Add(randomInUnitSphere()))
	scattered = Ray{rec.p, target.Sub(rec.p)}
	attenuation = m.albedo
	valid = true
	return
}

// Scatter funciton for the metal material.
func (m Metal) Scatter(r Ray, rec HitRecord) (valid bool, attenuation Vec3, scattered Ray) {
	reflected := reflect(r.direction.Unit(), rec.normal)
	scattered = Ray{rec.p, reflected.Add(randomInUnitSphere().Mul(m.fuzz))}
	attenuation = m.albedo
	valid = scattered.direction.Dot(rec.normal) > 0
	return
}

// Scatter funciton for the dielectric material.
func (m Dielectric) Scatter(r Ray, rec HitRecord) (valid bool, attenuation Vec3, scattered Ray) {
	var outwardNormal Vec3
	var refractiveFactor float64
	var cosine float64
	if r.direction.Dot(rec.normal) > 0 {
		outwardNormal = rec.normal.Mul(-1.0)
		refractiveFactor = m.refractiveIndex
		cosine = m.refractiveIndex * r.direction.Dot(rec.normal) / r.direction.Norm()
	} else {
		outwardNormal = rec.normal
		refractiveFactor = 1.0 / m.refractiveIndex
		cosine = -r.direction.Dot(rec.normal) / r.direction.Norm()
	}

	valid = true
	attenuation = m.albedo
	var validRefraction bool
	var refracted, reflected Vec3
	var reflectProbability float64
	if validRefraction, refracted = refract(r.direction, outwardNormal, refractiveFactor); validRefraction {
		reflectProbability = schlick(cosine, m.refractiveIndex)
	} else {
		reflectProbability = 1.0
	}

	if rand.Float64() < reflectProbability {
		reflected = reflect(r.direction.Unit(), rec.normal)
		scattered = Ray{rec.p, reflected}
	} else {
		scattered = Ray{rec.p, refracted}
	}

	return
}
